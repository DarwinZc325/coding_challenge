class Protocol {

   constructor() {
      this.size_buffer = 30 // tamaño de bloques que tiene el buffer, si se requiere se puede cambiar
      this.buffer = []
      this.listRequests = [] // Lista de peticiones acumuladas
   }

   // llenar la buffer con bloques de información
   mockFillBuffer () {
      for (let i = 0; i <= this.size_buffer; i++) {
         this.buffer[i] = {
            'index': i,
            'name': `pos_${i+1}`
         }
      }
   }

   // Llenar una lista con peticiones para pruebas
   mockRequests() {
      // Si se requiere se puede agregar más peticiones o cambiar los valores de las que ya hay
      this.listRequests = [
         [30, 1, 'cb1'],
         [9, 2, 'cb2'],
         [0, 4, 'cb3']
      ]
   }

   /**
    * Toma los parámetros dados y ejecuta un callback para obtener un payload
    * @param start => index donde se inicia la sección a tomar
    * @param length => longitud de la sección a tomar
    * @param callback => función que ontiene la sección requirida
    */
   read (start, length, callback) {
      const lengthBuffer = this.buffer.length

      // start no debe ser mayor al índice máximo del buffer
      if (start > (lengthBuffer - 1)) {
         callback(`start no debe ser mayor a ${(lengthBuffer - 1)}`, null)
         return
      }

      // length no deber ser mayor a la cantidad de elementos que hay a partir de start
      if (length > (lengthBuffer - start)){
         callback(`La longitud no debe ser mayor ${(lengthBuffer - start)}`, null)
         return
      }

      const buff =  this.buffer.slice(start, start + length)
      callback(null, buff)
   }

   // Recorrer la lista de peticiones y obtener los payloads
   resolveRequests () {

      return this.listRequests.map(req => {
         const start = req[0]
         const length = req[1]
         const cb = req[2]
         let results // Obtener el payload para poder ser retornado.
         this.read(start, length, (err, payload) => {
            if (err) {
               console.error(err)
               return
            }
            results = payload
         })
         return results
      })
   }

   // llamar y ejecurat los métodos
   main() {
      // iniciar los mocks con datos para pruebas
      this.mockFillBuffer()
      this.mockRequests()

      // mostrar el buffer principal
      console.group('Inicio buffer')
      console.log(this.buffer, '\n')
      console.groupEnd()

      // mostrar los payloads resultantes.
      console.group('Payloads resultantes')

      // recorrer los payloads obtenidos e imprimirlos
      this.resolveRequests().forEach((res, i) => {
         console.log('Payload', (i + 1))
         console.log(res, '\n')
      })
   }
}

// Instanciar la clase Protocol y ejecutar el método main
(new Protocol()).main()


